\documentclass[12pt,a4paper]{article}

\include{header}

\setlength{\oddsidemargin}{-5 mm}

\newtheorem{satz}{Satz}

\hypersetup{pdfinfo = {
  Title = Fibonacci im Aufzug,
  Author = Andreas Geyer-Schulz,
  Subject = Pi-Day 2009}
}

\begin{document}
\begin{center}
{\huge Fibonacci im Aufzug}

\vspace{5mm}
{\Large 14.~März 2009 -- {\em $\pi$-Day}}

\vspace{5mm}
{\large Andreas Geyer-Schulz}
\end{center}

Als ich vor einiger Zeit mit dem Aufzug in den 11.~Stock des Karlsruher Physikhochhauses fahren wollte blieb dieser plötzlich zwischen 5. und 6.~Stock stehen und eine rote Lampe ging an.

Bevor wir uns jedoch den Geheimnissen des Karlsruher Physikhochhausaufzugs (im folgenden kurz \glqq Aufzug\grqq\ genannt) widmen, möchte ich an eine von Leonardo von Pisa -- genannt Fibonacci -- im Jahr 1202 vorgelegte Aufgabe zur Kaninchenvermehrung erinnern (vgl. \cite[Seite 283]{sdlp}):

\begin{quote}
  \emph{Jemand brachte ein Kaninchenpaar an einen gewissen, allseits von Wänden umgebenen Ort, um herauszufinden, wieviel [Paare] aus diesem Paar in einem Jahr entstehen würden.
  Es sei die Natur der Kaninchen, pro Monat ein neues Paar hervorzubringen und im zweiten Monat nach der Geburt [erstmals] zu gebären.
  [Todesfälle mögen jedoch nicht eintreten.]}
\end{quote}

\begin{figure}[hb]
  \includegraphics[width = 150mm]{fibonacci.eps}
  \caption{Die Hauptpersonen: Leonardo von Pisa, genannt Fibonacci, \emph{Brachylagos Idahoenis}, das Physikhochhaus der Universität Karlsruhe (TH)}
\end{figure}

Ob dieses Modell wirklich realistisch ist, soll uns an dieser Stelle nicht weiter kümmern, wir werden es mathematisch untersuchen.
Wenn also ein Urpaar unmittelbar nach der Geburt eingesperrt wird, so haben wir im ersten Monat $1$~Paar, im zweiten Monat immer noch $1$~Paar, im dritten Monat $1+1$~Paare, im vierten Monat $1+2 = 3$~Paare und so weiter.
Bezeichnet man die Anzahl der Kaninchenpaare im $n$-ten Monat mit $\bf{F_n}$, so gilt offensichtlich
\begin{equation} \tag{$\star$}
  \begin{split}
    F_1 & = 1 , \quad F_2 = 1 \\
    \text{sowie} \quad F_{n+1} & = F_n + F_{n-1}.
    \label{fibz}
  \end{split}
\end{equation}
Die auf diese Weise definierte Zahlenfolge nennt man die {\bf Fibonaccifolge}, benannt nach ihrem Erfinder.
Ihre ersten Folgenglieder lauten also:
\begin{equation*}
  1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, \ldots\footnote{Man
beachte, dass man in der Mathematik \glqq und so weiter\grqq\
nur dann sagen darf, wenn man wirklich weiß, wie es weitergeht.}
\end{equation*}

Das wirklich überraschende ist, dass diese einfache Entdeckung eine unglaubliche Anzahl weiterer Entdeckungen hervorgebracht hat.
Es sind soviele, dass es seit 1963 sogar eine Zeitschrift gibt, \emph{The Fibonacci Quaterly}, die sich mit nichts anderem als Fibonaccizahlen beschäftigt.
S.~L.~Basin schreibt in seinem Aufsatz \cite{fsan}:
\glqq \emph{The Fibonacci numbers have the strange habit of appearing where least expected.}\grqq\ Dies wird sich auch im Physikhochhaus bestätigen.

Es ist eine nicht zu verleugnende Tatsache, dass die Aufzüge im Physikhochhaus vollkommen unterdimensioniert sind.
Um diesem Konstruktionsfehler abzuhelfen, ließ man sich etwas einfallen und befestigte in den Aufzügen ein wunderbares Schild mit der Aufschrift:
\begin{quote}
\boxed{\emph{Bitte drücken Sie keine benachbarten Stockwerksnummern.}}
\end{quote}

Wenn man öfters in den 11.~Stock fahren möchte, dabei jedes Mal 5 Minuten wartet bis endlich ein Aufzug kommt und dann mit vielen anderen, die sich nicht immer an diese Regel halten, hinauf fährt, so kommt man unweigerlich auf folgende Fragestellung:
Wie viele verschiedene Aufzugsfahrten, die im Erdgeschoss starten und keine benachbarten Stockwerke ansteuern, gibt es?
Und weil man Mathematiker ist, betrachtet man auch nicht das $16$-stöckige \glqq echte\grqq\ Physikhochhaus, sondern gleich ein $n$ stöckiges.

Die {\bf Anzahl der möglichen Fahrten} im n-stöckigen Hochhaus wird im folgenden mit $\bf{A_n}$ bezeichnet.
($A_n$ sind dann die \glqq Aufzugszahlen\grqq.)
In Abbildung~\ref{an} werden die Aufzugszahlen für $n = 1,2,3$ berechnet.
Man beachte, dass wir die triviale Fahrt, bei der der Aufzug einfach gar nicht fährt, mitzählen, außerdem erlauben wir die Fahrt vom Erdgeschoß in den ersten Stock.

\begin{figure}
  \begin{center}
    \includegraphics[width = 100mm]{aufzugsz.eps}
  \end{center}
  \caption{Die Werte von $A_1$, $A_2$ und $A_3$ (die blauen Kreuze zeigen die Stockwerke, in denen der Aufzug hält).}
  \label{an}
\end{figure}

Wie sieht das aber im Allgemeinen aus?
Nehmen wir an, dass wir $A_n$ schon berechnet haben.
Wie kommen wir nun auf $A_{n+1}$?
Zum einen gibt es die Möglichkeit, dass wir gar nicht in den $n+1$-ten Stock fahren, dafür gibt es (das haben wir ja schon berechnet) $A_n$ Möglichkeiten.
Wenn wir nun doch in den $n+1$-ten Stock fahren wollen, müssen wir aufpassen, denn eine Fahrt vom $n$-ten in den $n+1$-ten Stock ist nicht erlaubt (wenn man sich an die Regeln hält).
Aber wir können an jede Fahrt, die in den $n-1$-ten Stock führt (das sind insgesamt $A_{n-1}$ Stück), einfach den $n+1$-ten anhängen, dann haben wir alle Möglichkeiten.
Es ergibt sich also:
\begin{equation*}
  A_{n+1} = A_n + A_{n-1}.
\end{equation*}
Das erinnert natürlich an Gleichung~\eqref{fibz} und zusammen mit der Anfangsbedingung $A_1 = 2, A_2 = 3$ aus Abbildung~\ref{an} ergibt sich:

\begin{satz} \label{satz1}
  Für alle $n\in\N$ gilt $A_n = F_{n+2}$.
\end{satz}

Damit ist das Problem auf ganz überraschende Weise gelöst, aber man kann noch mehr herausholen.
Es gibt nämlich noch eine weitere Möglichkeit, sich die Anzahl der Aufzugsfahrten zu überlegen.

Dazu müssen wir zunächst einen kurzen Ausflug zum Mississippi unternehmen.
Das bekannte \glqq Mississippi Problem\grqq\ fragt, auf wie viele Arten man die Buchstaben des Wortes {\tt MISSISSIPPI} umordnen kann,
         \marginpar{zB: \\
        {\tt MSISISISPPI \\
             SISISIISPMP \\
             ISISPIMSISP \\
             SIPIIPSMSIS \\
             ...}}
ohne dass dabei 2 {\tt S} nebeneinander stehen.
Das Wort enthält $4$ {\tt S}, $4$ {\tt I}, $2$ {\tt P} und $1$ {\tt  M}.
Zunächst überlegen wir uns, wie viele Möglichkeiten wir zur Verteilung der nicht-{\tt S} haben.
Für den ersten Buchstaben gibt es $7$ Möglichkeiten, für den zweiten $6$ und so weiter, insgesamt also $7!$.
Dabei haben wir jedoch nicht berücksichtigt, dass wir die $4$ {\tt I} und die $2$ {\tt P} gar nicht unterscheiden können, wir müssen also noch durch $4! \cdot 2!$ teilen und erhalten somit
\begin{equation*}
  \frac{7!}{4! \cdot 2!}
\end{equation*}
Möglichkeiten die $7$ nicht-{\tt S} anzuordnen.
Diese Buchstaben bilden nun $8$ Lücken und damit die {\tt S} nicht nebeneinander stehen, darf man in jede Lücke höchstens ein {\tt S} legen.
Da man auf $\binom{8}{4}$~Möglichkeiten $4$ aus $8$ auswählen kann, ist die Lösung des Mississippi Problems:
\begin{equation*}
  \binom{8}{4} \cdot \frac{7!}{4! \cdot 2!} = 7350.
\end{equation*}

Nun wieder zurück in den Aufzug.
Man könnte sich auch fragen, wie viele Möglichkeiten es gibt, dass der Aufzug im $n$-stöckigen Gebäude genau $k$~mal hält.
Dabei kann $k$ natürlich nur Werte zwischen $0$ und
$\lceil\frac{n}{2}\rceil$ annehmen\footnote{$\lceil x\rceil$ bedeutet dabei die kleinste ganze Zahl größer gleich als $x$, z.B.: $\lceil4\rceil = 4, \lceil\sqrt{5}\rceil = 3, \lceil-\pi\rceil = -3$.
Analog bedeutet $\lfloor x\rfloor$ die größte ganze Zahl kleiner oder gleich $x$.}.
Wie beim Mississippi-Problem bilden nun die Stockwerke, in denen wir nicht halten wollen $n-k+1$~Lücken, auf die wir die $k$~Halte verteilen müssen.
Also gibt es dafür $\binom{n-k+1}{k}$~Möglichkeiten.
Summieren wir nun über alle möglichen $k$, so ergibt sich die Anzahl aller Möglichkeiten und die haben wir ja schon ausgerechnet.
Formal erhalten wir also folgende verblüffende Identität:

\begin{satz}
  Für alle $n\in\N$ gilt
  \begin{equation*}
  A_n = \sum_{k = 0}^{\lceil\frac{n}{2}\rceil} \binom{n-k+1}{k}.
  \end{equation*}
  Aus Satz~\ref{satz1} und eine Indexverschiebung erhält man eine
  der unzähligen bekannten Identitäten der Fibonaccizahlen:
  \begin{equation} \label{fibeq} \tag{$\dagger$}
    F_{n+1} = \sum_{k = 0}^{\lfloor\frac{n}{2}\rfloor} \binom{n-k}{k}.
  \end{equation}
\end{satz}

Für alle die bisher gelesen haben, sei nun auch die Geschichte vom Anfang fertig erzählt.
Die rote Lampe stellte sich als Anzeige für \glqq Dienstfahrt\grqq\ heraus, dies bedeutete, dass uns der Hausmeister ferngesteuert in den Keller entführte, wo er mit einer riesigen Gasflasche einsteigen wollte.
Als er sah, dass der Aufzug recht voll war, meinte er: \glqq \emph{3 Minuten habts noch.}\grqq\ und schickte uns wieder nach oben.
Ob er wohl wusste, wie viele Möglichkeiten es gab, uns nach oben zu schicken?

Ich jedenfalls wünsche einen schönen {\bf $\pi$-Day 2009!}

Einladungen, Beschwerden, Kritik sind an \href{mailto:gsa@posteo.de}{gsa@posteo.de} zu richten.

\section*{Freiwillige Zusatzaufgabe}
Man beweise das Auftreten der Fibonaccizahlen im Pascalschen Dreieck, so wie in Abbildung~\ref{pascal} gezeigt.
(Hinweis: Gleichung \eqref{fibeq} könnte helfen.)

\begin{figure}
  \begin{center}
    \includegraphics[width = 60mm]{pascal.eps}
  \end{center}
  \caption{Auch im Pascalschen Dreieck findet man die Fibonaccizahlen.}
  \label{pascal}
\end{figure}


\begin{thebibliography}{10}
  \bibitem{fsan}
    {\sc S. L. Basin}:
    {\em The Fibonacci sequence as it appears in nature.}
    \newblock
    {Fibonacci Quaterly {\bf 1}, Seiten 53-56, 1963.}
  \bibitem{sdlp}
    {\sc Baldassare Boncompagni}
    (Hrsg.): {\em Scritti di Leonardo Pisano.}
    \newblock
    {Roma, 1857, vol 1.}
  \bibitem{heuser}
    {\sc Harro Heuser}:
    {\em Lehrbuch der Analysis, Teil 1.}
    \newblock {B.G. Teubner, Stuttgart, 1994.}
\end{thebibliography}

\end{document}
